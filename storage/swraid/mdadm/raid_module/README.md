# storage/swraid suite

This test is to check the kernel mod: raid0 rai1 raid456 raid10

Test Maintainer: [Fan Fan](mailto:ffan@redhat.com)

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
